COMMANDE_COMPILATION="javac -classpath ./classes -sourcepath ./src -d ./classes"
if [[ $# > 0 ]]
then
  FichiersJava=""
  for Parametre in $*
    do
      FichiersJava="$FichiersJava $(find -name *.java | grep $Parametre)"
    done
else FichiersJava=$(find -name *.java)
fi
for Fichier in $FichiersJava
do
  $COMMANDE_COMPILATION $Fichier
done
