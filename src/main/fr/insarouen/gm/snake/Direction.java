package fr.insarouen.gm.snake;

public enum Direction
// énumération des directions possible pour le serpent + un élément neutre "SANS"
{
  HAUT,
  BAS,
  DROITE,
  GAUCHE,
  SANS
}
