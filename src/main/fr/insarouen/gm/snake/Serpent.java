package fr.insarouen.gm.snake;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Serpent
//classe modélisant le serpent avec sa direction actuelle, sa taille et l'ensemble des positions constitiant son corps
{
  //attributs
  private Direction dir;
  private int taille;
  private LinkedList<Position> corps;

  //constructeurs
  public Serpent(Position posInit) {
    dir = Direction.HAUT;
    taille = 3;
    corps = new LinkedList<Position>();

    corps.add(new Position(posInit.getLigne(), posInit.getColonne())); //création de la tête du serpent

    for (int i = 1; i < taille ; i++) {
      corps.addLast(new Position(posInit.getLigne() + i, posInit.getColonne())); //on ajoute la suite du corp du serpent sous la tête
    }
  }

  //accesseurs
  public Direction getDir()
  {
    return dir;
  }

  public int getTaille()
  {
    return taille;
  }

  public LinkedList<Position> getCorps() {
    return corps;
  }

  //mutateurs
  public void setDir(Direction d)
  {
    dir = d;
  }

  public void setTaille(int t)
  {
    taille = t;
  }

  //Méthodes
  public Position positionTete()
  //renvoie la position de la tête du serpent
  {
    Position posTete;
    try {
      posTete = corps.getFirst();
    }
    catch(NoSuchElementException e) {
      posTete = new Position(-1, -1);
      System.out.println("Le serpent est vide");
    }

    return posTete;
  }

  public void changerDirection(Direction direction)
  //permet de faire changer de direction au serpent en vérifiant que la direction en entrée est valide
   {
      if (direction == Direction.HAUT && this.getDir() != Direction.BAS) {
          this.setDir(direction);
      }
      else if (direction == Direction.BAS && this.getDir() != Direction.HAUT) {
          this.setDir(direction);
      }
      else  if (direction == Direction.GAUCHE && this.getDir() != Direction.DROITE) {
          this.setDir(direction);
      }
      else  if (direction == Direction.DROITE && this.getDir() != Direction.GAUCHE) {
          this.setDir(direction);
      }
  }

  public void augmenter()
  //augmente la taille du serpent et le déplace en fonction de la direction
  {
    Position posTete = positionTete();
    Position pos = new Position(posTete.getLigne(), posTete.getColonne());

    switch (dir) {
      case HAUT :
        pos.setLigne(pos.getLigne() - 1);
        break;
      case BAS :
        pos.setLigne(pos.getLigne() + 1);
        break;
      case DROITE :
        pos.setColonne(pos.getColonne() + 1);
        break;
      case GAUCHE :
        pos.setColonne(pos.getColonne() - 1);
        break;
    }

    corps.addFirst(pos);  //ajoute la nouvelle position en tête
    setTaille(getTaille() + 1);
  }

  public void deplacer()
  //déplace le serpent en fonction de la direction et en conservant la taille
  {
    augmenter();
    setTaille(getTaille() - 1);
    corps.removeLast();
  }

  public boolean estPresent(Position pos)
  //renvoie vrai si le serpent se trouve sur la position passée en entrée
  {
    return corps.contains(pos);
  }

  public String toString()
  {
    return ("taille = " + getTaille() + "\n direction = " + getDir() + "\n corps = " + corps);
  }
}
