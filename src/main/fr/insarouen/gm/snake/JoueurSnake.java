package fr.insarouen.gm.snake;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class JoueurSnake extends Joueur
// classe qui hérite de la classe Joueur et qui s'adapte au cas du joueur du jeu snake
{
    //attributs
    private Direction action;


    //constructeurs
    public JoueurSnake()
    {
        action = Direction.SANS;
    }

    //accesseurs
    public Direction getAction()
    {
        return this.action;
    }

    public void setAction(Direction direction)
    {
        this.action = direction;
    }

    public Direction obtenirAction()
    {
        return this.getAction();
    }


    @Override
    public void actionPerformed(ActionEvent e) {} //pas utilisé dans notre cas

    @Override
    public void keyPressed(KeyEvent e) //est appellé quand une touche du clavier est enfoncée
    {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_Q)
        {
            setAction(Direction.GAUCHE) ;
        } else if (key == KeyEvent.VK_D)
        {
            setAction(Direction.DROITE) ;
        } else if (key == KeyEvent.VK_Z)
        {
            setAction(Direction.HAUT) ;
        } else if (key == KeyEvent.VK_S)
        {
            setAction(Direction.BAS) ;
        } else
        {
            setAction(Direction.SANS) ; //si aucune des touches qui nous intéressent n'est enfoncée on renvoi SANS
        }
    }
}
