package fr.insarouen.gm.snake;
import javax.swing.JFrame;


public abstract class JeuUnJoueur extends JFrame
//classe abstraite pouvant servir de base pour créer des classes modélisant tous types de jeu à un joueur
{
  //attributs
  private Joueur unJoueur;

  //accesseur
  public Joueur getJoueur()
  {
      return unJoueur ;
  }
  //mutateur
  public void setJoueur(Joueur leJoueur)
  {
      unJoueur = leJoueur ;
  }

  //Méthodes abstraites
  public abstract boolean fini();  //renvoie vrai si la partie est finie
  public abstract void afficherJeu();
  public abstract void tourDeJeu(); //exécute un tour de jeu avec réalisation d'une action + affichage de l'état du jeu
  public abstract boolean victoire(); //renvoie vrai si le joueur a gagné la partie

  //Méthode implémentée
  public String lancerJeu()
  //réalise les tours de jeu jusqu'à ce que la partie soit finie. Retourne une phrase de victoire ou défaite à afficher
  {
    do {
      tourDeJeu();
    } while (!fini());

    String phraseConclu;
    if (victoire()) {
      phraseConclu = "Vous avez gagné !";
    }
    else{
      phraseConclu = "Vous avez perdu...";
    }
    return phraseConclu;
  }
}
