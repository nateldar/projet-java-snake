package fr.insarouen.gm.snake;

import java.util.Objects;

public class Position
    //classe modélisant une position sur une surface de dimension 2 avec un numéro de ligne et un numéro de colonne
{
    //attributs
    private int ligne;
    private int colonne;

    //constructeurs
    public Position() {
        ligne = 0;
        colonne = 0;
    }

    public Position(int l, int c) {
        ligne = l;
        colonne = c;
    }

    //accesseurs
    public int getLigne ()
    {
        return ligne;
    }

    public int getColonne()
    {
        return colonne;
    }

    public void setLigne(int l)
    {
        ligne = l;
    }

    public void setColonne(int c)
    {
        colonne = c;
    }

    //redefinition To String
    public String toString()
    {
        return ("(ligne :" + this.getLigne() + ", colonne :"+ this.getColonne() +")");
    }

    //methodes
    @Override
    public boolean equals(Object o)
    //on redéfinit equals pour pouvoir utiliser les methodes de bases sur les LinkedList
    {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof Position)) {
            return false;
        }
        Position position = (Position) o;
        return getLigne() == position.getLigne() &&
                getColonne() == position.getColonne();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLigne(), getColonne());
    }
}
