package fr.insarouen.gm.snake;

import java.awt.event.KeyAdapter;
import java.awt.event.ActionListener;


public abstract class Joueur extends KeyAdapter implements ActionListener
//classe abstraite pouvant servir de base pour créer des classes modélisant les joueurs de différents jeux
{
     //methodes
     public abstract <ClasseAction> ClasseAction obtenirAction();
}
