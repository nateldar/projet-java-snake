package fr.insarouen.gm.snake;
import java.util.Random;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
import java.lang.InterruptedException;


public class JeuSnake extends JeuUnJoueur
{
  //attributs
  private Serpent unSerpent;
  private int hauteur;
  private int largeur;
  private Position fruit;
  private int tailleVictoire;
  private static final int HAUT_DEFAUT = 15;
  private static final int LARG_DEFAUT = 15;
  private static final int TAILLE_VICTOIRE_DEFAUT = 20;
  private static final int TAILLE_CASE = 15;

  // marge pour le calcul de la taille de la fenetre
  private static final int MARGE_HAUT = 4;
  private static final int MARGE_LARG = 2;

  // décallage des éléments graphiques pour qu'ils soient centrés
  private static final int DECAL_HAUT = 3;
  private static final int DECAL_LARG = 1;

  // constructeur
  public JeuSnake(int hauteur, int largeur, int tailleVictoire){
    this.hauteur = hauteur;
    this.largeur = largeur; //initialisation des dimensions du jeu

    this.tailleVictoire = tailleVictoire;

    this.setJoueur(new JoueurSnake());
    Position posInit = new Position(largeur/2 , hauteur/2); //calcul la position initiale du serpent (au milieux)
    unSerpent = new Serpent(posInit);
    ajouterFruit();


    setSize(TAILLE_CASE *(getLargeur() + MARGE_LARG), TAILLE_CASE *(getHauteur() + MARGE_HAUT)); //taille de la fenêtre
    addKeyListener(this.getJoueur()); //permet récupérer l'état du clavier


    setResizable(true);
    setTitle("Jeu Snake");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //options de la JFrame

    repaint(); //affiche l'état du jeu sur la fenetre
    setVisible(true); //la fenetre deviens visible

  }

  //accesseurs
  public int getHauteur() {
    return hauteur;
  }

  public int getLargeur() {
    return largeur;
  }

  public int getTailleVictoire() {
    return tailleVictoire;
  }

  public Position getFruit() {
    return fruit;
  }

  public Serpent getSerpent() {
    return unSerpent;
  }

  //mutateurs
  public void sethauteur(int hauteur) {
    this.hauteur = hauteur;
  }

  public void setLargeur(int largeur) {
    this.largeur = largeur;
  }

  public void setTailleVictoire() {
    this.tailleVictoire = tailleVictoire;
  }

  public void setFruit(Position fruit) {
    this.fruit = fruit;
  }

  public void setSerpent(Serpent unSerpent) {
    this.unSerpent = unSerpent;
  }


  //Méthodes
  @Override
  public boolean victoire() {
    return !collision(); //le joueur à gagné si la fin du jeu n'est pas causée par une collision du serpent
  }

  @Override
  public boolean fini() {
    return (collision() || unSerpent.getTaille() >= tailleVictoire);
  }

  //Ces methodes permettent d'obtenir la position d'un element sur la Jframe
  //à partir de sa position dans le Jeu
  public int redimensionnerLargeur(int posX) {
    return (posX + DECAL_LARG ) * TAILLE_CASE;
  }

  public int redimensionnerHauteur(int posY){
    return (posY + DECAL_HAUT ) * TAILLE_CASE;
  }

  // Met à jour l'affichage du jeu (est appelée par repaint())
  @Override
  public void paint(Graphics graph) {
      super.paint(graph);
      graph.setColor(Color.blue);
      graph.drawRect(redimensionnerLargeur(0), redimensionnerHauteur(0), (getLargeur())* TAILLE_CASE, (getHauteur())* TAILLE_CASE);
      dessinerFruit(graph);
      dessinerSerpent(graph);
  }

  public void dessinerSerpent(Graphics graph) {
      Position pos;
      graph.setColor(Color.green.darker());

      for (int i = 0; i < this.getSerpent().getTaille(); i++) {
          pos = this.getSerpent().getCorps().get(i);
          graph.fillOval(redimensionnerLargeur(pos.getColonne()), redimensionnerHauteur(pos.getLigne()), TAILLE_CASE, TAILLE_CASE);
      }
  }

  public void dessinerFruit(Graphics graph) {
      Position pos = this.getFruit();
      graph.setColor(Color.red);
      graph.fillOval(redimensionnerLargeur(pos.getColonne()), redimensionnerHauteur(pos.getLigne()), TAILLE_CASE, TAILLE_CASE);

  }


  @Override
  public void afficherJeu()
  {
      repaint(); //permet de mettre à jour l'affichage de la Jframe
  }

  @Override
  public void tourDeJeu() {
    afficherJeu();
    unSerpent.changerDirection(this.getJoueur().obtenirAction());

    if (estPositionFruit(unSerpent.positionTete())) {
      mangerFruit();    //manger un fruit fait avancer le Seprent donc on n'a pas besoin d'appeler deplacer()
    }
    else {
      unSerpent.deplacer();
    }

    try {
        Thread.sleep(250); //bonne vitesse : 250 ms de pause
    }
    catch (InterruptedException exception) {
        System.out.println(exception);
    }
  }

  public boolean collision() {  //vérifie si la tête du serpent est dans un mur ou si elle apparait une deuxième fois dans le corp du serpent
    Position posTete = unSerpent.positionTete();

    return (posTete.getLigne() > (getHauteur()-1) || posTete.getLigne() < 0)
            || (posTete.getColonne() > (getLargeur()-1) || posTete.getColonne() < 0)
            || (unSerpent.getCorps().lastIndexOf(posTete) > 1);
  }

  public boolean estPositionFruit(Position pos) {
    return pos.equals(fruit);
  }

  public void ajouterFruit() {
    Position nouveauFruit = new Position();
    Random generateur = new Random();

    do {
      nouveauFruit.setLigne(generateur.nextInt(getHauteur()));
      nouveauFruit.setColonne(generateur.nextInt(getLargeur()));    //on génère une position dans les limites du jeu
  } while (unSerpent.estPresent(nouveauFruit)); //on vérifie que le fruit n'apparait pas dans le Serpent
    fruit = nouveauFruit;
  }

  public void mangerFruit() {
    unSerpent.augmenter();
    ajouterFruit();
  }

  public static void main (String args[]) {
    JeuSnake jeu = new JeuSnake(HAUT_DEFAUT, LARG_DEFAUT, TAILLE_VICTOIRE_DEFAUT);
    String issueDuJeu = jeu.lancerJeu();

    JOptionPane.showMessageDialog(null, issueDuJeu, "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
  }
}
