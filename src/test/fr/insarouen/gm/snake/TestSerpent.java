package fr.insarouen.gm.snake;

class TestSerpent {
  public static void main(String args[]) {
    Position pos;
    pos = new Position(2, 3);
    Serpent serp;

    // Test constructeurs
    System.out.println("Test constructeur :");
    serp = new Serpent(pos);
    System.out.println(serp);

    //test estPresent
    System.out.println("TEST estPresent :");
    System.out.println("avec "+ pos + " : " + serp.estPresent(pos));
    pos = new Position(5,5);
    System.out.println("avec "+ pos + " : " + serp.estPresent(pos));

    //test ChangerDirection :
    System.out.println("TEST changerDirection :");
    serp.setDir(Direction.HAUT);

    System.out.println("Changement dans la même direction (vers le HAUT): ");
    serp.changerDirection(Direction.HAUT);
    System.out.println(serp.getDir());
    System.out.println("Changement Normal (vers la GAUCHE): ");
    serp.changerDirection(Direction.GAUCHE);
    System.out.println(serp.getDir());
    System.out.println("Changement impossible (vers la DROITE): ");
    serp.changerDirection(Direction.DROITE);
    System.out.println(serp.getDir());




    // test positionTete
    System.out.println("TEST positionTete() :");
    System.out.println(serp.positionTete());


    //test déplacer
    System.out.println("TEST deplacer() :");
    System.out.println(serp.getCorps());
    System.out.println("Vers le HAUT :");
    serp.setDir(Direction.HAUT);
    serp.deplacer();
    System.out.println(serp.getCorps());
    System.out.println("Vers la DROITE :");
    serp.setDir(Direction.DROITE);
    serp.deplacer();
    System.out.println(serp.getCorps());
    System.out.println("Vers le BAS :");
    serp.setDir(Direction.BAS);
    serp.deplacer();
    System.out.println(serp.getCorps());
    System.out.println("Vers la GAUCHE :");
    serp.setDir(Direction.GAUCHE);
    serp.deplacer();
    System.out.println(serp.getCorps());


    // test augmenter
    System.out.println("TEST augmenter() :");
    System.out.println(serp);
    System.out.println("Vers le HAUT :");
    serp.setDir(Direction.HAUT);
    serp.augmenter();
    System.out.println(serp);
    System.out.println("Vers la DROITE :");
    serp.setDir(Direction.DROITE);
    serp.augmenter();
    System.out.println(serp);
    System.out.println("Vers le BAS :");
    serp.setDir(Direction.BAS);
    serp.augmenter();
    System.out.println(serp);
    System.out.println("Vers la GAUCHE :");
    serp.setDir(Direction.GAUCHE);
    serp.augmenter();
    System.out.println(serp);


  }
}
