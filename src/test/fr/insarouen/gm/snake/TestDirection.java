package fr.insarouen.gm.snake;

class TestDirection {
  public static void main(String args[]) {
    Direction h, b, d, g, s;
    h = Direction.HAUT;
    b = Direction.BAS;
    d = Direction.DROITE;
    g = Direction.GAUCHE;
    s = Direction.SANS;
    System.out.println("h = " + h);
    System.out.println("b = " + b);
    System.out.println("d = " + d);
    System.out.println("g = " + g);
    System.out.println("s = " + s);
  }
}
