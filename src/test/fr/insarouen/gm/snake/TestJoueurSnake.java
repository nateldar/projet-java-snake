package fr.insarouen.gm.snake;
import javax.swing.*;
public class TestJoueurSnake extends JFrame
{
    public TestJoueurSnake (JoueurSnake j1)
    {
        setSize(500, 500);
        addKeyListener(j1);
        setResizable(true);
        setTitle("Test JoueurSnake");
        setLocationRelativeTo(null); // pas nécessaire
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    public static void main (String args[])
    {
        JoueurSnake j1;
        //constructeurs et affichage :
        System.out.println("TEST CONSTRUCTEURS :");
        j1 = new JoueurSnake();
        System.out.println("action initiale :" + j1.getAction());



        //accesseurs :
        System.out.println("TEST ACCESSEURS :");
        j1.setAction(Direction.HAUT);
        System.out.println("action changée :" + j1.getAction());
        j1.setAction(Direction.SANS);

        //methodes :

        TestJoueurSnake test = new TestJoueurSnake(j1) ;



        while (j1.obtenirAction() != Direction.HAUT) {}
        JOptionPane.showMessageDialog(null, "vous avez appuyer sur Z", "Test réussi", JOptionPane.INFORMATION_MESSAGE);
        while (j1.obtenirAction() != Direction.BAS) {}
        JOptionPane.showMessageDialog(null, "vous avez appuyer sur S", "Test réussi", JOptionPane.INFORMATION_MESSAGE);
        while (j1.obtenirAction() != Direction.DROITE) {}
        JOptionPane.showMessageDialog(null, "vous avez appuyer sur D", "Test réussi", JOptionPane.INFORMATION_MESSAGE);
        while (j1.obtenirAction() != Direction.GAUCHE) {}
        JOptionPane.showMessageDialog(null, "vous avez appuyer sur Q", "Test réussi", JOptionPane.INFORMATION_MESSAGE);


    }

}
