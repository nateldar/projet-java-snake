package fr.insarouen.gm.snake;

public class TestPosition
{
    public static void main (String args[])
    {
        Position p1, p2 ;
        //constructeurs et affichage :
        System.out.println("TEST CONSTRUCTEURS :");
        p1 = new Position();
        p2 = new Position(4,5);
        System.out.println("p1 :" + p1);
        System.out.println("p2 :" + p2);

        //accesseurs :
        System.out.println("TEST ACCESSEURS :");
        System.out.println("p2 : l = " + p2.getLigne() + " c = " + p2.getColonne());
        p1.setColonne(2);
        p1.setLigne(3);
        System.out.println("p1 : " + p1);
        //methodes :
        System.out.println("TEST METHODES :");
        System.out.println("methode equals");
        System.out.println("p1 = p2 : " + p1.equals(p2));
        System.out.println("p1 = p1 : " + p1.equals(p1));
    }

}
